# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
   prov - a project to do datacenter/application deployment, management in modern way

* Version
   0.0001 :)
### How do I get set up? ###

### Summary of set up
  You may install it in virtual env by following below steps

* install python-virtualenv

```
    $ apt-get install python-virtualenv 
    Reading package lists... Done
    Building dependency tree
```

* Run "virtualenv venv" on your workspace

```
    $ virtualenv venv
    New python executable in venv/bin/python
    Installing setuptools, pip...done.
```
    
* Activate your virtual env 

```

    $ source venv/bin/activate
    (venv)root@code1:~/test$
    
    (venv)root@code1:~/test# pip install -U pip
    Downloading/unpacking pip from https://pypi.python.org/packages/py2.py3/p/pip/pip-8.0.2-py2.py3-none-any.whl#md5=2056f553d5b593d3a970296f229c1b79
      Downloading pip-8.0.2-py2.py3-none-any.whl (1.2MB): 1.2MB downloaded
    Installing collected packages: pip
      Found existing installation: pip 1.5.4
        Uninstalling pip:
          Successfully uninstalled pip
    Successfully installed pip
    Cleaning up...
```

* Install python-prov using "pip install -e git+https://bitbucket.org/to3/python-prov.git#egg=prov"

```

    (venv)root@code1:~/test# pip install -e git+https://bitbucket.org/to3/python-prov.git#egg=prov
    Obtaining prov from git+https://bitbucket.org/to3/python-prov.git#egg=prov
      Cloning https://bitbucket.org/to3/python-prov.git to ./venv/src/prov
    Username for 'https://bitbucket.org': hkumarmk
    Password for 'https://hkumarmk@bitbucket.org': 
    Collecting pyYAML (from prov)
    /root/test/venv/local/lib/python2.7/site-packages/pip/_vendor/requests/packages/urllib3/util/ssl_.py:315: SNIMissingWarning: An HTTPS request has been made, but the SNI (Subject Name Indication) extension to TLS is not available on this platform. This may cause the server to present an incorrect TLS certificate, which can cause validation failures. For more information, see https://urllib3.readthedocs.org/en/latest/security.html#snimissingwarning.
      SNIMissingWarning
    /root/test/venv/local/lib/python2.7/site-packages/pip/_vendor/requests/packages/urllib3/util/ssl_.py:120: InsecurePlatformWarning: A true SSLContext object is not available. This prevents urllib3 from configuring SSL appropriately and may cause certain SSL connections to fail. For more information, see https://urllib3.readthedocs.org/en/latest/security.html#insecureplatformwarning.
      InsecurePlatformWarning
      Using cached PyYAML-3.11.tar.gz
    Collecting shade (from prov)
      Downloading shade-1.4.0-py2-none-any.whl (169kB)
        100% |████████████████████████████████| 172kB 303kB/s 
    Collecting mock (from prov)
      Using cached mock-1.3.0-py2.py3-none-any.whl
    Collecting python-digitalocean (from prov)
      Downloading python-digitalocean-1.8.tar.gz
    Collecting ndg-httpsclient (from prov)
      Downloading ndg_httpsclient-0.4.0.tar.gz
    Collecting python-ironicclient>=0.10.0 (from shade->prov)
    
    ...............
    ...............
    ...............
      Running setup.py install for dogpile.core ... done
      Running setup.py install for dogpile.cache ... done
      Running setup.py install for munch ... done
      Running setup.py install for python-digitalocean ... done
      Running setup.py install for enum34 ... done
      Running setup.py install for pycparser ... done
      Running setup.py install for cffi ... done
      Running setup.py install for cryptography ... done
      Running setup.py install for ndg-httpsclient ... done
      Running setup.py develop for prov
    Successfully installed Babel-2.2.0 PrettyTable-0.7.2 PyOpenSSL-0.15.1 anyjson-0.3.3 appdirs-1.4.0 cffi-1.5.0 cliff-1.16.0 cmd2-0.6.8 cryptography-1.2.2 debtcollector-1.3.0 decorator-4.0.9 dogpile.cache-0.5.7 dogpile.core-0.4.1 enum34-1.1.2 funcsigs-0.4 functools32-3.2.3.post2 futures-3.0.4 httplib2-0.9.2 idna-2.0 ipaddress-1.0.16 iso8601-0.1.11 jsonpatch-1.12 jsonpointer-1.10 jsonschema-2.5.1 keystoneauth1-2.2.0 mock-1.3.0 monotonic-0.6 msgpack-python-0.4.7 munch-2.0.4 ndg-httpsclient-0.4.0 netaddr-0.7.18 netifaces-0.10.4 openstacksdk-0.7.4 os-client-config-1.14.0 oslo.config-3.6.0 oslo.i18n-3.3.0 oslo.serialization-2.3.0 oslo.utils-3.5.0 pbr-1.8.1 prov pyYAML-3.11 pyasn1-0.1.9 pycparser-2.14 pyparsing-2.1.0 python-cinderclient-1.5.0 python-digitalocean-1.8 python-glanceclient-1.2.0 python-heatclient-0.9.0 python-ironicclient-1.1.0 python-keystoneclient-2.1.2 python-neutronclient-4.0.0 python-novaclient-3.2.0 python-openstackclient-2.1.0 python-swiftclient-2.7.0 python-troveclient-2.0.0 pytz-2015.7 requests-2.9.1 requestsexceptions-1.1.3 shade-1.4.0 simplejson-3.8.1 six-1.10.0 stevedore-1.11.0 unicodecsv-0.14.1 warlock-1.2.0 wrapt-1.10.6
    /root/test/venv/local/lib/python2.7/site-packages/pip/_vendor/requests/packages/urllib3/util/ssl_.py:120: InsecurePlatformWarning: A true SSLContext object is not available. This prevents urllib3 from configuring SSL appropriately and may cause certain SSL connections to fail. For more information, see https://urllib3.readthedocs.org/en/latest/security.html#insecureplatformwarning.
      InsecurePlatformWarning
    (venv)root@code1:~/test#
```

* now on you may just activate the virtual env by following #3 and use python-prov

```

    (venv)root@code1:~/test# deactivate    # To get out of virtualenv
    
    root@code1:~/test# source venv/bin/activate	# To get into virtualenv
    (venv)root@code1:~/test# 

```

* you may upgrade it using pip install -U <url> (see above point for the url)

* you will then be able to use the commands defined in setup.py (specified by "entrypoints=") under python-prov source (https://bitbucket.org/to3/python-prov/src/3b2ab5a1d073ff9e244a44a383938d01a0a2d464?at=master) 
	for example now one command has been specified there - as "provisioner", you will be able to run "privisioner -h" within virtualenv now.

```

    (venv)root@code1:~/test# provisioner -h
    usage: provisioner [-h] [-p PROVIDER] {network,server} ...
    
    Provision infrastructure systems
    
    positional arguments:
      {network,server}      Resource to manage
        network             manage networks
        server              manage server
    
    optional arguments:
      -h, --help            show this help message and exit
      -p PROVIDER, --provider PROVIDER
                            provider type, Default: openstack

```

## Configuration
## Dependencies
## How to run tests
## Deployment instructions

### Contribution guidelines ###

##  Writing tests
## Code review
## Other guidelines

### Who do I talk to? ###

## Repo owner or admin
## Other community or team contact
